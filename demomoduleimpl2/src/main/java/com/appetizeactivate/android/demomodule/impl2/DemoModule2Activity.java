package com.appetizeactivate.android.demomodule.impl2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.appetizeactivate.android.demomodule.contract.DemoModuleUIContract;

public class DemoModule2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo_module2);


        // retrieving parameters from intent
        Bundle params = getIntent().getExtras();
        int int1 = params.getInt(DemoModuleUIContract.EXTRA_MODULE_UI_INT1_INPUT, 0);
        int int2 = params.getInt(DemoModuleUIContract.EXTRA_MODULE_UI_INT2_INPUT, 0);

        final int sum = int1 + int2;

        ((TextView) findViewById(R.id.sum_view)).setText(""+sum);

        findViewById(R.id.fabSum).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent data = new Intent();
                data.putExtra(DemoModuleUIContract.EXTRA_MODULE_UI_SUM_OUTPUT, sum);
                setResult(RESULT_OK, data);
                finish();
            }
        });
    }
}
