package com.appetizeactivate.android.demomodule.impl1;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.appetizeactivate.android.demomodule.contract.DemoModuleUIContract;
import com.jakewharton.rxbinding2.view.RxView;
import com.jakewharton.rxbinding2.widget.RxTextView;

import io.reactivex.disposables.Disposable;

public class DemoModuleUIActivity extends AppCompatActivity {

    private Disposable clickHandle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo_module_ui);

        // retrieving parameters from intent
        Bundle params = getIntent().getExtras();
        int int1 = params.getInt(DemoModuleUIContract.EXTRA_MODULE_UI_INT1_INPUT, 0);
        int int2 = params.getInt(DemoModuleUIContract.EXTRA_MODULE_UI_INT2_INPUT, 0);

        int sum = int1 + int2;

        ((TextView) findViewById(R.id.sum_view)).setText(""+sum);

        clickHandle = RxView.clicks(findViewById(R.id.button)).map(ignored ->
        {
            Intent result = new Intent();
            result.putExtra(DemoModuleUIContract.EXTRA_MODULE_UI_SUM_OUTPUT, sum);
            return result;
        }).subscribe(
                intent -> {
                    setResult(RESULT_OK, intent);
                    finish();
                }
        );

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        clickHandle.dispose();
    }
}
