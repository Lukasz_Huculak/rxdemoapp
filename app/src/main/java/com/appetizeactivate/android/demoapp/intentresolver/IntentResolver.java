package com.appetizeactivate.android.demoapp.intentresolver;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.os.Bundle;

/**
 * Created by lukaszhuculak on 19.06.2017.
 */

public final class IntentResolver {

    private final Activity activity;

    public IntentResolver(Activity activity) {
        this.activity = activity;
    }

    public void startActivity(String action, Bundle extras) {
        Intent intent = createIntent(action, extras);
        intent = resolveIntent(intent);
        activity.startActivity(intent);
    }

    private Intent createIntent(String action, Bundle extras) {
        Intent intent = new Intent(action);
        intent.putExtras(extras);
        intent.setPackage(activity.getPackageName());
        return intent;
    }

    private Intent resolveIntent(Intent intent) {
        ResolveInfo activityResolved = activity.getPackageManager().resolveActivity(intent, 0);
        if (activityResolved == null || activityResolved.activityInfo == null ) {
            throw new ActivityNotFoundException("Could not find activity for action '"+intent.getAction()+"'");
        }
        intent.setClassName(activity, activityResolved.activityInfo.name);
        return intent;
    }


    public void startActivityForResult(int requestCode, String action, Bundle extras) {

        Intent intent = createIntent(action, extras);
        intent = resolveIntent(intent);
        activity.startActivityForResult(intent, requestCode);
    }
}
