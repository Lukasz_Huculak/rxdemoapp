package com.appetizeactivate.android.demoapp.swiper;

import com.appetizeactivate.android.demoapp.DemoActivity;
import com.appetizeactivate.android.demoapp.logs.LoggingConsumer;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.Subject;

/**
 * Created by lukaszhuculak on 19.06.2017.
 */

public enum SwiperManager {
    INSTANCE;


    private Observable<CardData> currentStream;

    public Observable<CardData> getDataFromSwiper() {
        synchronized (SwiperManager.class) {
            if (currentStream == null) {
                currentStream = Observable.interval(2, 5, TimeUnit.SECONDS)
                        .subscribeOn(Schedulers.io())
                        .map(ignored -> System.currentTimeMillis())
                        .doOnNext(new LoggingConsumer<Long>(DemoActivity.TAG, "data from swiper = "))
                        .map(timestamp -> new CardData(timestamp, ""+timestamp));
            }
        }
        return currentStream.publish().refCount();
    }

    public static class CardData {
        public final long timestamp;
        public final String payload;

        public CardData(long timestamp, String payload) {
            this.timestamp = timestamp;
            this.payload = payload;
        }
    }

}
