package com.appetizeactivate.android.demoapp.model;

/**
 * Created by lukaszhuculak on 19.06.2017.
 */

public class DemoData {
    public final long _id;
    public final long timestamp;
    public final int retries;
    public final boolean synced;
    public final String payload;

    public DemoData(long id, long timestamp, int retries, boolean synced, String payload) {
        _id = id;
        this.timestamp = timestamp;
        this.retries = retries;
        this.synced = synced;
        this.payload = payload;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("DemoData{");
        sb.append("_id=").append(_id);
        sb.append(", timestamp=").append(timestamp);
        sb.append(", retries=").append(retries);
        sb.append(", synced=").append(synced);
        sb.append(", payload='").append(payload).append('\'');
        sb.append('}');
        return sb.toString();
    }
}

