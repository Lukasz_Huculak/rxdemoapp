package com.appetizeactivate.android.demoapp.logs;

import android.util.Log;

import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;

/**
 * Created by lukaszhuculak on 19.06.2017.
 */

public class LoggingConsumer<T> implements Consumer<T>, Action {
    private final int logLevel;
    private final String tag;
    private final String prefix;

    public LoggingConsumer(String tag, String prefix) {
        this(Log.DEBUG, tag, prefix);
    }


    public LoggingConsumer(int logLevel, String tag, String prefix) {

        this.logLevel = logLevel;
        this.tag = tag;
        this.prefix = prefix;
    }

    @Override
    public void accept(@NonNull T t) throws Exception {
        Log.println(logLevel, tag, prefix + t);
    }

    @Override
    public void run() throws Exception {
        Log.println(logLevel, tag, prefix);
    }
}
