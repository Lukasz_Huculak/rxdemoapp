package com.appetizeactivate.android.demoapp.io;

import android.util.Log;
import android.util.Pair;

import com.appetizeactivate.android.demoapp.logs.LoggingConsumer;
import com.appetizeactivate.android.demoapp.model.DemoData;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;

/**
 * Created by lukaszhuculak on 20.06.2017.
 */

public class SyncProvider {

    private static final String TAG = "SyncProv";

    private Database dbHandle;
    private ExecutorService syncExecutor;
    private Disposable queueDisposable = null;

    public void setMultithreaded(boolean multithreaded) {
        if (this.multithreaded != multithreaded) {
            this.multithreaded = multithreaded;
            if (syncInProgressState.getValue()){
                //restart
                changeSyncState(false);
                changeSyncState(true);
            }
        }
    }

    private boolean multithreaded = false;


    private BehaviorSubject<Boolean> syncInProgressState = BehaviorSubject.createDefault(false);
    private Subject<Pair<DemoData, Boolean>> dataFlowState = PublishSubject.create();


    public SyncProvider(Database database) {
        dbHandle = database;
    }

    public Observable<Boolean> syncInProgress() {
        return syncInProgressState;
    }

    public Observable<Pair<DemoData, Boolean>> dataSyncStateChange() {
        return dataFlowState;
    }

    public void changeSyncState(boolean doRun) {

        if (!doRun && syncInProgressState.getValue()) {
            if (queueDisposable != null) {
                queueDisposable.dispose();
                queueDisposable = null;
            }
            syncExecutor.shutdown();
            syncInProgressState.onNext(Boolean.FALSE);
            Log.d(TAG, "toggleSync: stopped");
        }

        if (doRun && !syncInProgressState.getValue()) {
            prepareExecutorService();
            queueDisposable = dbHandle.getDataToSync().distinctUntilChanged((cur, next) -> cur._id == next._id && cur.timestamp == next.timestamp)
                    .doOnNext(new LoggingConsumer<DemoData>(TAG, "data to sync = "))
                    .subscribe(
                            demoData -> {
                                dataFlowState.onNext(new Pair<>(demoData, true));
                                syncExecutor.execute(new DataSynchronizer(demoData));
                            }
                    );
            syncInProgressState.onNext(Boolean.TRUE);
            Log.d(TAG, "toggleSync: started");
        }
    }

    private void prepareExecutorService() {
        if (syncExecutor == null || syncExecutor.isShutdown()) {
            synchronized (SyncProvider.class) {
                if (syncExecutor == null || syncExecutor.isShutdown()) {
                    if (multithreaded) {
                        syncExecutor = Executors.newCachedThreadPool();
                    } else {
                        syncExecutor = Executors.newSingleThreadExecutor();
                    }
                }
            }
        }
    }

    private class DataSynchronizer implements Runnable {
        private final DemoData data;

        public DataSynchronizer(DemoData demoData) {
            this.data = demoData;
        }

        @Override
        public void run() {
            Log.d(TAG, Thread.currentThread().getName()+" "+"run() called " + data._id);
            Random r = new Random();
            synchronized (this) {
                try {
                    this.wait(r.nextInt(2000) + 2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            if (r.nextInt(10) > 5) {
                dbHandle.updateRow(data._id, data.retries, true);
            } else {
                dbHandle.updateRow(data._id, data.retries + 1, false);
            }

            dataFlowState.onNext(new Pair<>(data, false));
        }
    }
}
