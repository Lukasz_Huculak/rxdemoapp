package com.appetizeactivate.android.demoapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.appetizeactivate.android.demoapp.intentresolver.IntentResolver;
import com.appetizeactivate.android.demoapp.logs.LoggingConsumer;
import com.appetizeactivate.android.demoapp.swiper.SwiperManager;
import com.appetizeactivate.android.demomodule.contract.DemoModuleUIContract;
import com.jakewharton.rxbinding2.support.design.widget.RxBottomNavigationView;
import com.jakewharton.rxbinding2.view.RxView;
import com.jakewharton.rxbinding2.widget.RxTextView;
import com.jakewharton.rxbinding2.widget.TextViewEditorActionEvent;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;

public class DemoActivity extends AppCompatActivity {

    public static final String TAG = "DEMO";
    private static final int GET_SUM_REQUEST_CODE = 0xbaba;
    private CompositeDisposable disposables = new CompositeDisposable();
    private TextView cardnumberText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo);

        final EditText num1ET = (EditText) findViewById(R.id.num1);
        final EditText num2ET = (EditText) findViewById(R.id.num2);

        findViewById(R.id.sumBtn).setOnClickListener(view -> {
            int num1 = 0, num2 = 0;
            try {
                num1 = Integer.parseInt(num1ET.getText().toString());
                num2 = Integer.parseInt(num2ET.getText().toString());
            } catch (Exception ignored) {
            }

            Bundle params = new Bundle();
            params.putInt(DemoModuleUIContract.EXTRA_MODULE_UI_INT1_INPUT, num1);
            params.putInt(DemoModuleUIContract.EXTRA_MODULE_UI_INT2_INPUT, num2);
            new IntentResolver(DemoActivity.this).startActivityForResult(GET_SUM_REQUEST_CODE, DemoModuleUIContract.ACTION_SHOW_MODULE_UI, params);
        });


        Observable<Integer> selectedInput = RxBottomNavigationView.itemSelections((BottomNavigationView) findViewById(R.id.navigation))
                .subscribeOn(AndroidSchedulers.mainThread())

                .map(MenuItem::getItemId)
                .doOnNext(new LoggingConsumer<>(TAG, "selected item = "))
                .publish().refCount();

        cardnumberText = (TextView) findViewById(R.id.cardNumber);


        disposables.add(selectedInput.map(itemId -> itemId == R.id.entry_manual) // manual item selected
                .doOnNext(new LoggingConsumer<>(TAG, "MANUAL = "))
                .subscribe(RxView.enabled(cardnumberText))); // enable/disable depending on selected menu item

        disposables.add(selectedInput
                .switchMap(menuItemId -> {
                    switch (menuItemId) {
                        case R.id.entry_manual:
                            return cardNumberFromManualInput();
                        case R.id.entry_swiper:
                            return cardNumberFromSwiper();
                        case R.id.entry_chip:
                            return cardNumberFromChip();
                    }
                    return Observable.empty();
                }).observeOn(AndroidSchedulers.mainThread())
                .subscribe(cardnumber -> {
                    Toast.makeText(DemoActivity.this, "Number: " + cardnumber, Toast.LENGTH_SHORT).show();
                }));

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        disposables.dispose();
    }

    private Observable<String> cardNumberFromSwiper() {
        return SwiperManager.INSTANCE.getDataFromSwiper()
                .map(cardData -> cardData.payload);
    }

    private Observable<String> cardNumberFromChip() {
        return Observable.empty();
    }

    private Observable<String> cardNumberFromManualInput() {
        Log.d(TAG, "cardNumberFromManualInput() called");
        return RxTextView.editorActionEvents(cardnumberText, textViewEditorActionEvent -> textViewEditorActionEvent.actionId() == EditorInfo.IME_ACTION_DONE)
                .map(TextViewEditorActionEvent::view)
                .map(TextView::getText)
                .map(CharSequence::toString)
                .doOnNext(new LoggingConsumer<String>(TAG, "entered card no = "));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            int sum = data.getIntExtra(DemoModuleUIContract.EXTRA_MODULE_UI_SUM_OUTPUT, 0);
            ((TextView) findViewById(R.id.sumValue)).setText("" + sum);
        }
    }
}
