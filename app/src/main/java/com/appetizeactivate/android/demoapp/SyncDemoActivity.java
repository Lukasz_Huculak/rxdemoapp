package com.appetizeactivate.android.demoapp;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;

import com.appetizeactivate.android.demoapp.io.Database;
import com.appetizeactivate.android.demoapp.io.SyncProvider;
import com.appetizeactivate.android.demoapp.logs.LoggingConsumer;
import com.jakewharton.rxbinding2.view.RxView;
import com.jakewharton.rxbinding2.widget.RxCompoundButton;
import com.jakewharton.rxbinding2.widget.RxProgressBar;
import com.jakewharton.rxbinding2.widget.RxTextView;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class SyncDemoActivity extends AppCompatActivity {

    private static final String TAG = "SyncDEMO";
    private Database database;
    private CompositeDisposable disposables = new CompositeDisposable();
    private SyncProvider syncProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sync_demo);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        EditText dataEntry = (EditText) findViewById(R.id.dataEntry);

        TextView dataCountTextView = (TextView) findViewById(R.id.allCount);
        TextView dataInQueueCountTextView = (TextView) findViewById(R.id.queueCount);

        Switch toggleSync = (Switch) findViewById(R.id.syncSwitch);
        Switch toggleMultiThreadedSync = (Switch) findViewById(R.id.multithreadedSwitch);
        ProgressBar progressBar = (ProgressBar) findViewById(R.id.syncProgress);

        database = new Database(getApplicationContext());

        syncProvider = new SyncProvider(database);

        //binding ui
        disposables.add(
                database.countData().observeOn(AndroidSchedulers.mainThread())
                        .map(Object::toString)
                        .subscribe(RxTextView.text(dataCountTextView)));

        disposables.add(
                database.countDataInQueue().observeOn(AndroidSchedulers.mainThread())
                        .map(Object::toString)
                        .subscribe(RxTextView.text(dataInQueueCountTextView)));

        disposables.add(syncProvider.syncInProgress()
                .doOnNext(new LoggingConsumer<Boolean>(TAG, "sync in progress = "))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(RxView.visibility(progressBar))
        );
        disposables.add(database.countData()
                .observeOn(AndroidSchedulers.mainThread())
                .map(Long::intValue)
                .subscribe(RxProgressBar.max(progressBar)));
        disposables.add(Observable.combineLatest(database.countData(), database.countDataInQueue(), (all, inQueue) -> all - inQueue) // in real-life scenario this should be calculated by database
                .map(Long::intValue)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(RxProgressBar.progress(progressBar)));

        Observable<CharSequence> newDataStream = RxView.clicks(fab).flatMapSingle(ignored -> RxTextView.textChanges(dataEntry).firstOrError()).publish().refCount();

        Consumer<String> dataInserter = payload -> database.insertRow(payload);

        //adding data
        disposables.add(newDataStream.filter(data -> !TextUtils.isEmpty(data)).filter(TextUtils::isDigitsOnly) // digits only
                .map(cs -> Long.parseLong(cs.toString()))
                .observeOn(Schedulers.single())
                .flatMap(count -> Observable.rangeLong(0, count))
                .map(index -> "New entry " + index)
                .subscribe(dataInserter));

        disposables.add(newDataStream.filter(data -> !TextUtils.isEmpty(data))
                .filter(data -> !TextUtils.isDigitsOnly(data)) // non-digits only
                .observeOn(Schedulers.single())
                .map(Object::toString)
                .subscribe(dataInserter));


        // toggle sync
        disposables.add(RxCompoundButton.checkedChanges(toggleSync).subscribe(state -> syncProvider.changeSyncState(state)));

        //toggle multithreaded
        disposables.add(RxCompoundButton.checkedChanges(toggleMultiThreadedSync).subscribe(state -> syncProvider.setMultithreaded(state)));

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        disposables.dispose();
    }
}
