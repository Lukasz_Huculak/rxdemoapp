package com.appetizeactivate.android.demoapp.io;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.appetizeactivate.android.demoapp.model.DemoData;
import com.squareup.sqlbrite2.BriteDatabase;
import com.squareup.sqlbrite2.SqlBrite;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;


/**
 * Created by lukaszhuculak on 19.06.2017.
 */

public class Database {

    private static final String TABLE_NAME = "demo_data";
    private final BriteDatabase briteDb;

    public Database(Context context) {
        this.briteDb = new SqlBrite.Builder().build().wrapDatabaseHelper(new DBLocalHelper(context), Schedulers.io());
    }

    public void updateRow(long id, int retries, boolean synced) {

        ContentValues cv = new ContentValues();
        cv.put("timestamp", System.currentTimeMillis());
        cv.put("retries", retries);
        cv.put("synchronized", synced ? 1 : 0);
        briteDb.update(TABLE_NAME, cv, "_id = ?", "" + id);

    }

    public Long insertRow(String payload) {
        ContentValues cv = new ContentValues();
        cv.put("timestamp", System.currentTimeMillis());
        cv.put("payload", payload);
        long id = briteDb.insert(TABLE_NAME, cv);
        return id;
    }

    public Observable<DemoData> getDataToSync() {
        return briteDb.createQuery(TABLE_NAME, "SELECT * FROM " + TABLE_NAME + " WHERE synchronized = ? ORDER BY timestamp LIMIT 1", "0").mapToOne(cursor -> new DemoData(
                cursor.getLong(cursor.getColumnIndex("_id"))
                , cursor.getLong(cursor.getColumnIndex("timestamp"))
                , cursor.getInt(cursor.getColumnIndex("retries"))
                , cursor.getInt(cursor.getColumnIndex("synchronized")) > 0
                , cursor.getString(cursor.getColumnIndex("payload"))
        ));
    }

    public Observable<Long> countDataInQueue() {
        return briteDb.createQuery(TABLE_NAME, "SELECT count(_id) FROM " + TABLE_NAME + " WHERE synchronized = ?", "0").mapToOneOrDefault(cursor -> cursor.getLong(0), 0L);
    }

    public Observable<Long> countData() {
        return briteDb.createQuery(TABLE_NAME, "SELECT count(_id) FROM " + TABLE_NAME, new String[]{}).mapToOneOrDefault(cursor -> cursor.getLong(0), 0L);
    }


    private class DBLocalHelper extends SQLiteOpenHelper {
        public DBLocalHelper(Context context) {
            super(context, "DEMO_DB", null, 1);
        }

        @Override
        public void onCreate(SQLiteDatabase sqLiteDatabase) {
            sqLiteDatabase.execSQL("CREATE TABLE " + TABLE_NAME + " (_id INTEGER PRIMARY KEY, timestamp INTEGER, payload TEXT, retries INTEGER DEFAULT 0, synchronized INTEGER DEFAULT 0);");
        }

        @Override
        public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
            sqLiteDatabase.execSQL("DROP TABLE " + TABLE_NAME);
            onCreate(sqLiteDatabase);
        }
    }
}
