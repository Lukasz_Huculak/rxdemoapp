package com.appetizeactivate.android.demomodule.contract;

/**
 * Created by lukaszhuculak on 19.06.2017.
 */

public interface DemoModuleUIContract {

    String ACTION_SHOW_MODULE_UI = "com.appetizeactivate.android.demomodule.contract.action.SHOW_MODULE_UI";

    //INPUT VALUES
    /**
     * Input extra - first number
     *
     * Type: Integer
     */
    String EXTRA_MODULE_UI_INT1_INPUT = "com.appetizeactivate.android.demomodule.contract.extra.INT1";
    /**
     * Input extra - second number
     *
     * Type: Integer
     */
    String EXTRA_MODULE_UI_INT2_INPUT = "com.appetizeactivate.android.demomodule.contract.extra.INT2";


    // OUTPUT VALUES
    /**
     * Result output extra - second number
     *
     * Type: Integer
     */
    String EXTRA_MODULE_UI_SUM_OUTPUT = "com.appetizeactivate.android.demomodule.contract.extra.SUM";

}
